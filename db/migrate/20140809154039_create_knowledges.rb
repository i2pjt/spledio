class CreateKnowledges < ActiveRecord::Migration
  def change
    create_table :knowledges do |t|
      t.string :knowledge_title, :null => false
      t.text :knowledge_contents, :null => false
      t.string :knowledge_type, :null => false, :limit => 2
      t.string :product_id
      t.integer :create_user_id, :null => false
      t.integer :update_user_id, :null => false
      t.timestamps
    end
  end
end
