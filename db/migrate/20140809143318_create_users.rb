class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :last_knj_name, :null => false, :limit => 10
      t.string :first_knj_name, :null => false, :limit => 10
      t.string :last_kana_name, :null => false, :limit => 20
      t.string :first_kana_name, :null => false, :limit => 20
      t.string :password, :null => false
      t.string :email, :null => false
      t.binary :image_file
      t.date :entered_date
      t.boolean :retire_flag
      t.string :certification_level, :limit => 2
      t.timestamps
    end
  end
end
