# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(:last_knj_name => '藤原', :first_knj_name => '愼一郎', :last_kana_name => 'フジワラ', :first_kana_name => 'シンイチロウ', :password => 'a3w21090', :email => 'Shinichiro_Fujiwara@jmas.co.jp', :entered_date => '2007-04-01', :retire_flag => false, :certification_level => '99')
