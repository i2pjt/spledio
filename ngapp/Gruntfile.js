// Generated on 2014-07-30 using generator-angular 0.9.5
'use strict';

module.exports = function(grunt) {

    // --------------------------------[environment settigns.]--------------------------------------

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);
    // How to Write Options expample
    // require('load-grunt-tasks')(grunt, {pattern: 'grunt-*'});
    // require('load-grunt-tasks')(grunt, {pattern: ['grunt-*', '!grunt-contrib-coffee']});
    // require('load-grunt-tasks')(grunt, {config: '../package'});
    // require('load-grunt-tasks')(grunt, {scope: ['devDependencies','depndencies']});

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Configurable paths for the application
    var appConfig = {
        //app: require('./bower.json').appPath || 'app',
        app: '../public',
        temp: 'temp'
    };

    var env = {
        jade: {
            srcdir: 'src/jade',
            destdir: '../public',
            testdir: 'test'
        },

        coffee: {
            srcdir: 'src/coffee',
            destdir: 'temp'
        },

        sass: {
            srcdir: 'src/sass'
        }
    };

    // --------------------------------[define tasks.]--------------------------------------

    // Define the configuration for all the tasks
    grunt.initConfig({

            // Metadata.
            pkg: grunt.file.readJSON('package.json'),

            // Project settings.
            yeoman: appConfig,

            // *** HTTP Server. ***
            connect: {
                server: {
                    options: {
                        port: 9900
                    }
                }
            },

            // *** File Watch.[grunt-contrib-watch] ***
            watch: {
                options: {
                    livereload: true,
                    spawn: false
                },

                jade: {
                    files: env.jade.srcdir + '/*.jade',
                    tasks: 'jade:compile'
                },

                coffee: {
                    files: env.coffee.srcdir + '/*.coffee',
                    tasks: ['coffee:compile', 'coffee:test']
                },

                compass: {
                    files: env.sass.srcdir + '/*.scss',
                    tasks: 'compass:compile'
                }
            },

            // *** Open File/Brower.[grunt-open] ***
            open: {
                dev: {
                    path: 'http://localhost:9000',
                    app: 'Google Chrome'
                }
            },

            // *** Comple to HTML.[grunt-contrib-jade] ***
            // [Options]
            //    pretty: 
            //    files: 
            jade: {
                compile: {
                    options: {
                        pretty: true
                    },
                    files: {
                        env.jade.destdir + '/index.html': env.jade.srcdir + '/index.jade',
                        env.jade.destdir + '/header.html': env.jade.srcdir + '/header.jade',
                        env.jade.destdir + '/footer.html': env.jade.srcdir + '/footer.jade'
                    }
                }
            },

            // *** Compile to JavaScript.[grunt-contrib-coffee] ***
            coffee: {
                compile: {
                    options: {
                        bare: true
                    },
                    expand: true,
                    cwd: env.coffee.srcdir,
                    src: '*.coffee',
                    dest: env.coffee.destdir + '/',
                    ext: '.js'
                },

                test: {
                    options: {
                        bare: true
                    },
                    expand: true,
                    cwd: env.coffee.srcdir,
                    src: '*.coffee',
                    dest: env.coffee.testdir + '/',
                    ext: '.js'

                }
            },

            // *** Compile to CSS.[grunt-contrib-compass] ***
            compass: {
                compile: {
                    options: {
                        config: 'config.rb'
                    }
                }
            },

            // *** File Copy.[grunt-contrib-copy] ***
            // [Options]
            //    expand: コピー先ディレクトリが存在しない場合に作成するにはtrueを指定
            //    cwd: コピーの起点となるディレクトリを指定
            //    src: コピー対象(指定したディレクトリもコピー対象)
            //    dest: コピー先ディレクトリ
            //    dot: コピー対象から「.」で始まる隠しファイルを対象にする場合、trueを指定
            copy: {
                dist: {
                    files: [
                        {
                            expand: true,
                            cwd: 'app/bower_components'
                            src: [
                                'angular/angular.min.js',
                                'angular-animate/angular-animate.min.js',
                                'angular-cookies/angular-cookies.min.js',
                                'angular-mocks/angular-mocks.js',
                                'angular-resource/angular-resource.min.js',
                                'angular-route/angular-route.min.js',
                                'angular-sanitize/angular-sanitize.min.js',
                                'angular-touch/angular-touch.min.js'
                            ],
                            dest: '../public/scripts'
                            dot: false
                        },
                        {
                            expand: true,
                            cwd: 'app/bower_components/bootstrap/dist/js',
                            src: 'bootstrap.min.js',
                            dest: '../public/scripts/bootstrap',
                            dot: false
                        },
                        {
                            expand: true,
                            cwd: 'app/bower_components/jquery/dist',
                            src: 'jquery.min.js',
                            dest: '../public/scripts/jquery',
                            dot: false
                        },
                        {
                            expand: true,
                            cwd: 'app/bower_components/json3/lib',
                            src: 'json3.min.js',
                            dest: '../public/scripts/json3',
                            dot: false
                        },
                        {
                            expand: true,
                            cwd: 'app/bower_components/bootstrap/dist/css',
                            src: 'bootstrap.min.css',
                            dest: '../public/styles',
                            dot: false
                        },
                        {
                            expand: true,
                            cwd: 'app/bower_components/bootstrap/dist/fonts',
                            src: 'glyphicons-halflings-regular.*'
                            dest: '../public/fonts'
                            dot: false
                        }
                    ]
                }
            },

            // *** Delete File.[grunt-contriby-clean] ***
            clean: {},

            // *** Check JS.[grunt-contriby-jshint] ***
            jshint: {
                options: {
                    jshintrc: '.jshintrc',
                    reporter: require('jshint-stylish')
                },
                product: {
                    src: [
                        'Gruntfile.js',
                        '<%= yeoman.app %>/scripts/{,*/}*.js'
                    ]
                },
                test: {
                    options: {
                        jshintrc: 'test/.jshintrc'
                    },
                src: ['test/spec/{,*/}*.js']
                }
            },

            // *** HTML minify.[grunt-contrib-htmlmin] ***
            htmlmin: {},

            // *** AngularJS minify.[grunt-ngmin] ***
            ngmin: {},

            // *** Javascript minify.[grunt-contriby-uglify] ***
            uglify: {},

            // *** CSS minify.[grunt-contrib-cssmin] ***
            cssmin: {},

            // *** Compressed Image File.[grunt-imagemin] ***
            imagemin: {},

            // *** Compressed SVG File.[grunt-svgmin] ***
            svgmin: {},

            // *** Divide Tasks.[grunt-concurrent] ***
            concurrent: {},

            // *** Create JS Document.[grunt-contrib-yuidoc] ***
            yuidoc: {},

            // *** JS Unit Test.[grunt-karma] ***
            karma: {
                unit: {
                    configFile: 'test/karma.conf.js',
                    singleRun: true
                }
            }

            // grunt-newer -> registerTaskでnewer:coffee:compileなど

        }
    });

// --------------------------------[Finished Refactoring]--------------------------------------

// // Watches files for changes and runs tasks based on the changed files
// watch: {
//     bower: {
//         files: ['bower.json'],
//         tasks: ['wiredep']
//     },
//     js: {
//         files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
//         tasks: ['newer:jshint:all'],
//         options: {
//             livereload: '<%= connect.options.livereload %>'
//         }
//     },
//     jsTest: {
//         files: ['test/spec/{,*/}*.js'],
//         tasks: ['newer:jshint:test', 'karma']
//     },
//     compass: {
//         files: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
//         tasks: ['compass:server', 'autoprefixer']
//     },
//     gruntfile: {
//         files: ['Gruntfile.js']
//     },
//     livereload: {
//         options: {
//             livereload: '<%= connect.options.livereload %>'
//         },
//         files: [
//             '<%= yeoman.app %>/{,*/}*.html',
//             '.tmp/styles/{,*/}*.css',
//             '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
//         ]
//     }
// },

// // Make sure code styles are up to par and there are no obvious mistakes
// jshint: {
//     options: {
//         jshintrc: '.jshintrc',
//         reporter: require('jshint-stylish')
//     },
//     all: {
//         src: [
//             'Gruntfile.js',
//             '<%= yeoman.app %>/scripts/{,*/}*.js'
//         ]
//     },
//     test: {
//         options: {
//             jshintrc: 'test/.jshintrc'
//         },
//         src: ['test/spec/{,*/}*.js']
//     }
// },

// // Empties folders to start fresh
// clean: {
//     dist: {
//         files: [{
//             dot: true,
//             src: [
//                 '.tmp',
//                 '<%= yeoman.dist %>/{,*/}*',
//                 '!<%= yeoman.dist %>/.git*'
//             ]
//         }]
//     },
//     server: '.tmp'
// },

// // Add vendor prefixed styles
// autoprefixer: {
//     options: {
//         browsers: ['last 1 version']
//     },
//     dist: {
//         files: [{
//             expand: true,
//             cwd: '.tmp/styles/',
//             src: '{,*/}*.css',
//             dest: '.tmp/styles/'
//         }]
//     }
// },

// // Automatically inject Bower components into the app
// wiredep: {
//     options: {
//         cwd: '<%= yeoman.app %>'
//     },
//     app: {
//         src: ['<%= yeoman.app %>/index.html'],
//         ignorePath: /\.\.\//
//     },
//     sass: {
//         src: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
//         ignorePath: /(\.\.\/){1,2}bower_components\//
//     }
// },

// // Renames files for browser caching purposes
// filerev: {
//     dist: {
//         src: [
//             '<%= yeoman.dist %>/scripts/{,*/}*.js',
//             '<%= yeoman.dist %>/styles/{,*/}*.css',
//             '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
//             '<%= yeoman.dist %>/styles/fonts/*'
//         ]
//     }
// },

// // Reads HTML for usemin blocks to enable smart builds that automatically
// // concat, minify and revision files. Creates configurations in memory so
// // additional tasks can operate on them
// useminPrepare: {
//     html: '<%= yeoman.app %>/index.html',
//     options: {
//         dest: '<%= yeoman.dist %>',
//         flow: {
//             html: {
//                 steps: {
//                     js: ['concat', 'uglifyjs'],
//                     css: ['cssmin']
//                 },
//                 post: {}
//             }
//         }
//     }
// },

// // Performs rewrites based on filerev and the useminPrepare configuration
// usemin: {
//     html: ['<%= yeoman.dist %>/{,*/}*.html'],
//     css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
//     options: {
//         assetsDirs: ['<%= yeoman.dist %>', '<%= yeoman.dist %>/images']
//     }
// },

// // The following *-min tasks will produce minified files in the dist folder
// // By default, your `index.html`'s <!-- Usemin block --> will take care of
// // minification. These next options are pre-configured if you do not wish
// // to use the Usemin blocks.
// // cssmin: {
// //   dist: {
// //     files: {
// //       '<%= yeoman.dist %>/styles/main.css': [
// //         '.tmp/styles/{,*/}*.css'
// //       ]
// //     }
// //   }
// // },
// // uglify: {
// //   dist: {
// //     files: {
// //       '<%= yeoman.dist %>/scripts/scripts.js': [
// //         '<%= yeoman.dist %>/scripts/scripts.js'
// //       ]
// //     }
// //   }
// // },
// // concat: {
// //   dist: {}
// // },

// imagemin: {
//     dist: {
//         files: [{
//             expand: true,
//             cwd: '<%= yeoman.app %>/images',
//             src: '{,*/}*.{png,jpg,jpeg,gif}',
//             dest: '<%= yeoman.dist %>/images'
//         }]
//     }
// },

// svgmin: {
//     dist: {
//         files: [{
//             expand: true,
//             cwd: '<%= yeoman.app %>/images',
//             src: '{,*/}*.svg',
//             dest: '<%= yeoman.dist %>/images'
//         }]
//     }
// },

// htmlmin: {
//     dist: {
//         options: {
//             collapseWhitespace: true,
//             conservativeCollapse: true,
//             collapseBooleanAttributes: true,
//             removeCommentsFromCDATA: true,
//             removeOptionalTags: true
//         },
//         files: [{
//             expand: true,
//             cwd: '<%= yeoman.dist %>',
//             src: ['*.html', 'views/{,*/}*.html'],
//             dest: '<%= yeoman.dist %>'
//         }]
//     }
// },

// // ngmin tries to make the code safe for minification automatically by
// // using the Angular long form for dependency injection. It doesn't work on
// // things like resolve or inject so those have to be done manually.
// ngmin: {
//     dist: {
//         files: [{
//             expand: true,
//             cwd: '.tmp/concat/scripts',
//             src: '*.js',
//             dest: '.tmp/concat/scripts'
//         }]
//     }
// },

// // Run some tasks in parallel to speed up the build process
// concurrent: {
//     server: [
//         'compass:server'
//     ],
//     test: [
//         'compass'
//     ],
//     dist: [
//         'compass:dist',
//         'imagemin',
//         'svgmin'
//     ]
// },
// });


grunt.registerTask('serve', 'Compile then start a connect web server', function(target) {
    if (target === 'dist') {
        return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
        'clean:server',
        'wiredep',
        'concurrent:server',
        'autoprefixer',
        'connect:livereload',
        'watch'
    ]);
});

grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function(target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
});

grunt.registerTask('test', [
    'clean:server',
    'concurrent:test',
    'autoprefixer',
    'connect:test',
    'karma'
]);

grunt.registerTask('build', [
    'clean:dist',
    'wiredep',
    'useminPrepare',
    'concurrent:dist',
    'autoprefixer',
    'concat',
    'ngmin',
    'copy:dist',
    'cdnify',
    'cssmin',
    'uglify',
    'filerev',
    'usemin',
    'htmlmin'
]);

grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
]);
};