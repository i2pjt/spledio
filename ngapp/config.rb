http_path = "../public/"
css_dir = "../public/styles"
sass_dir = "src/sass"
image_idr = "../public/images"
javascripts_dir = "../public/scripts"

environment = :dev
#environment = :product

cache = false
prefferred_syntax = :scss
output_style = (environment == :dev) ? :expanded :compressed
relative_assets = true
line_comments = false